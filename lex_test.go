// Code generated by [rec -lex=testLex -pkg main -import=unicode,unicode/utf8 <...>], DO NOT EDIT.

package main

import (
	"unicode"
	"unicode/utf8"
)

// testLex recognizes longest ASCII lexemes. Lower IDs take precedence on same length.
//
//	id   0: [a-z]+
//	id   1: [0-9]+
//
// ID == -1 is returned when no lexeme was recognized.
func testLex(s []byte) (id, length int) {
	const endOfText = 0x110000
	var pos, pos0, width, width1 int
	id = -1
	var r, r1 rune
	_ = pos0
	_ = r
	_ = r1
	_ = width1
	step := func(pos int) (rune, int) {
		if pos < len(s) {
			return rune(s[pos]), 1
		}
		return endOfText, 0
	}
	move := func() {
		pos += width
		if r, width = r1, width1; r != endOfText {
			r1, width1 = step(pos + width)
		}
	}
	accept := func(x rune) bool {
		if r == x {
			move()
			return true
		}
		return false
	}
	_ = accept
	accept2 := func(x rune) bool {
		if r <= x {
			move()
			return true
		}
		return false
	}
	_ = accept2
	r, r1 = endOfText, endOfText
	width, width1 = 0, 0
	r, width = step(pos)
	if r != endOfText {
		r1, width1 = step(pos + width)
	}
	if r < 'a' {
		goto l0out
	}
	if accept2('z') {
		goto l9
	}
l0out:
	if r < '0' {
		goto l2out
	}
	if accept2('9') {
		goto l5
	}
l2out:
	return id, length
l5:
	id, length = 1, pos
	if r < '0' {
		goto l6out
	}
	if accept2('9') {
		goto l5
	}
l6out:
	return id, length
l9:
	id, length = 0, pos
	if r < 'a' {
		goto l10out
	}
	if accept2('z') {
		goto l9
	}
l10out:
	return id, length
}

// testLexString recognizes longest ASCII lexemes. Lower IDs take precedence on same length.
//
//	id   0: [a-z]+
//	id   1: [0-9]+
//
// ID == -1 is returned when no lexeme was recognized.
func testLexString(s string) (id, length int) {
	const endOfText = 0x110000
	var pos, pos0, width, width1 int
	id = -1
	var r, r1 rune
	_ = pos0
	_ = r
	_ = r1
	_ = width1
	step := func(pos int) (rune, int) {
		if pos < len(s) {
			return rune(s[pos]), 1
		}
		return endOfText, 0
	}
	move := func() {
		pos += width
		if r, width = r1, width1; r != endOfText {
			r1, width1 = step(pos + width)
		}
	}
	accept := func(x rune) bool {
		if r == x {
			move()
			return true
		}
		return false
	}
	_ = accept
	accept2 := func(x rune) bool {
		if r <= x {
			move()
			return true
		}
		return false
	}
	_ = accept2
	r, r1 = endOfText, endOfText
	width, width1 = 0, 0
	r, width = step(pos)
	if r != endOfText {
		r1, width1 = step(pos + width)
	}
	if r < 'a' {
		goto l0out
	}
	if accept2('z') {
		goto l9
	}
l0out:
	if r < '0' {
		goto l2out
	}
	if accept2('9') {
		goto l5
	}
l2out:
	return id, length
l5:
	id, length = 1, pos
	if r < '0' {
		goto l6out
	}
	if accept2('9') {
		goto l5
	}
l6out:
	return id, length
l9:
	id, length = 0, pos
	if r < 'a' {
		goto l10out
	}
	if accept2('z') {
		goto l9
	}
l10out:
	return id, length
}

// testLexUtf8 recognizes longest UTF-8 lexemes. Lower IDs take precedence on same length.
//
//	id   0: \pL+
//	id   1: \p{Nd}+
//
// ID == -1 is returned when no lexeme was recognized.
func testLexUtf8(s []byte) (id, length int) {
	const endOfText = 0x110000
	var pos, pos0, width, width1 int
	id = -1
	var r, r1 rune
	_ = pos0
	_ = r
	_ = r1
	_ = width1
	step := func(pos int) (rune, int) {
		if pos < len(s) {
			c := s[pos]
			if c < utf8.RuneSelf {
				return rune(c), 1
			}
			return utf8.DecodeRune(s[pos:])
		}
		return endOfText, 0
	}
	move := func() {
		pos += width
		if r, width = r1, width1; r != endOfText {
			r1, width1 = step(pos + width)
		}
	}
	accept := func(x rune) bool {
		if r == x {
			move()
			return true
		}
		return false
	}
	_ = accept
	accept2 := func(x rune) bool {
		if r <= x {
			move()
			return true
		}
		return false
	}
	_ = accept2
	r, r1 = endOfText, endOfText
	width, width1 = 0, 0
	r, width = step(pos)
	if r != endOfText {
		r1, width1 = step(pos + width)
	}
	if unicode.Is(unicode.L, r) {
		move()
		goto l5
	}
	if unicode.Is(unicode.Nd, r) {
		move()
		goto l9
	}
	return id, length
l5:
	id, length = 0, pos
	if unicode.Is(unicode.L, r) {
		move()
		goto l5
	}
	return id, length
l9:
	id, length = 1, pos
	if unicode.Is(unicode.Nd, r) {
		move()
		goto l9
	}
	return id, length
}

// testLexStringUtf8 recognizes longest UTF-8 lexemes. Lower IDs take precedence on same length.
//
//	id   0: \pL+
//	id   1: \p{Nd}+
//
// ID == -1 is returned when no lexeme was recognized.
func testLexStringUtf8(s string) (id, length int) {
	const endOfText = 0x110000
	var pos, pos0, width, width1 int
	id = -1
	var r, r1 rune
	_ = pos0
	_ = r
	_ = r1
	_ = width1
	step := func(pos int) (rune, int) {
		if pos < len(s) {
			c := s[pos]
			if c < utf8.RuneSelf {
				return rune(c), 1
			}
			return utf8.DecodeRuneInString(s[pos:])
		}
		return endOfText, 0
	}
	move := func() {
		pos += width
		if r, width = r1, width1; r != endOfText {
			r1, width1 = step(pos + width)
		}
	}
	accept := func(x rune) bool {
		if r == x {
			move()
			return true
		}
		return false
	}
	_ = accept
	accept2 := func(x rune) bool {
		if r <= x {
			move()
			return true
		}
		return false
	}
	_ = accept2
	r, r1 = endOfText, endOfText
	width, width1 = 0, 0
	r, width = step(pos)
	if r != endOfText {
		r1, width1 = step(pos + width)
	}
	if unicode.Is(unicode.L, r) {
		move()
		goto l5
	}
	if unicode.Is(unicode.Nd, r) {
		move()
		goto l9
	}
	return id, length
l5:
	id, length = 0, pos
	if unicode.Is(unicode.L, r) {
		move()
		goto l5
	}
	return id, length
l9:
	id, length = 1, pos
	if unicode.Is(unicode.Nd, r) {
		move()
		goto l9
	}
	return id, length
}
