// Copyright 2023 The Rec Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Command rec compiles a subset of Go regular expressions to Go code.
//
// Installation
//
//	$ go install modernc.org/rec@latest
//
// # Benchmarks
//
// The numbers below are for the regexp
//
//	`sshd\[\d{5}\]:\s*Failed`
//
// matching input
//
//	`Jan 18 06:41:30 corecompute sshd[42327]: Failed keyboard-interactive/pam for root from 112.100.68.182 port 48803 ssh2`
//
// while starting at a different position on each invocation.
//
//	$ make bench
//	date > log-bench
//	go test -bench . 2>&1 | tee -a log-bench
//	goos: linux
//	goarch: amd64
//	pkg: modernc.org/rec
//	cpu: AMD Ryzen 9 3900X 12-Core Processor
//	BenchmarkMatch/stdlib-24         	13824402	        89.13 ns/op	       0 B/op	       0 allocs/op
//	BenchmarkMatch/modernc-24        	11125108	       102.7 ns/op	       0 B/op	       0 allocs/op
//	BenchmarkMatch/rec-24            	96561826	        13.44 ns/op	       0 B/op	       0 allocs/op
//	BenchmarkMatch/rec/string-24     	98201994	        13.18 ns/op	       0 B/op	       0 allocs/op
//	BenchmarkMatch/rec/utf8-24       	61928388	        18.22 ns/op	       0 B/op	       0 allocs/op
//	BenchmarkMatch/rec/stringutf8-24 	64053523	        17.74 ns/op	       0 B/op	       0 allocs/op
//	PASS
//	ok  	modernc.org/rec	12.038s
//	$
//
// # ASCII input
//
// The ASCII input is a byte sequence. Bytes 0x00 to 0xff are treated as
// unicode runes 0x00 to 0xff. This format is selected by flags not ending in
// utf8, for example '-match'.
//
// The ASCII input is slightly faster to process than Unicode input.
//
// # Unicode input
//
// The Unicode input format interprets the byte sequences as UTF-8 encoded. The
// UTF-8 input is selected by flags ending in utf8, for example '-matchutf8'.
//
// The Unicode input is slightly slower to process than ASCII input.
//
// # Usage example
//
// To run the compiler, invoke something like
//
//	$ rec -match=Match '\A(a|b)c\z'
//
// This will write a Go function definition (source code of)
//
//	func Match(s []byte) bool { ... }
//
// to stdout. The function will return the same result as
//
//	regexp.MustCompile(`\A(a|b)c\z`).Match(s)
//
// For the Lex* machines the arguments are regexps for one or more lexems, like
//
//	$ rec -lex=lex '[a-z]+' '[0-9]+'
//
// # List of option flags
//
// Flags that amend behavior.
//
// # The case insensitive flag
//
// Use -i to generate case insensitive machines.
//
// # The pkg flag
//
// Use -pkg to wrap the resulting function(s) in a package declaration. For example invoking
//
//	$ rec -match=Match -pkg=foo '\A(a|b)c\z'
//
// will write to stdout
//
//	package foo
//
//	func Match(s []byte) bool { ... }
//
// # The import flag
//
// Use -import to add a list of comma separated import paths, applicable only
// with -pkg. For example invoking
//
//	$ rec -match=Match -pkg=foo -import=bytes '\A(a|b)c\z'
//
// will write to stdout
//
//	package foo
//
//	import (
//		"bytes"
//	)
//
//	func Match(s []byte) bool { ... }
//
// # The rx flag
//
// Use -rx to make the produced machine become method of the flag argument,
// which must be a valid Go indentifier optionally prefixed with a single star
// ('*') character. For example invoking
//
//	$ rec -rx='*bar' -match=Match -pkg=foo -import=bytes '\A(a|b)c\z'
//
// will write to stdout
//
//	package foo
//
//	import (
//		"bytes"
//	)
//
//	func (*bar) Match(s []byte) bool { ... }
//
// # List of machine types
//
// All available machines are listed below.
//
// # The Lex machine
//
// Use -lex=<name> to produce a function implementing an ASCII []byte lexer,
// returning an regexp index and matched length in bytes or (-1, 0) if no
// lexeme was recognized.
//
// # The LexString machine
//
// Use -lexstring=<name> to produce a function implementing an ASCII string
// lexer, returning an regexp index and matched length in bytes or (-1, 0) if
// no lexeme was recognized.
//
// # The LexUtf8 machine
//
// Use -lexutf8=<name> to produce a function implementing an UTF8 []byte lexer,
// returning an regexp index and matched length in bytes or (-1, 0) if no
// lexeme was recognized.
//
// # The LexStringUtf8 machine
//
// Use -lexstringutf8=<name> to produce a function implementing an UTF8 string
// lexer, returning an regexp index and matched length in bytes or (-1, 0) if
// no lexeme was recognized.
//
// # The Match machine
//
// Use -match=<name> to produce a function returning the same result as
//
//	regexp.MustCompile(expr).Match(s)
//
// except the Match machine interprets 's' as ASCII.
//
// # The MatchString machine
//
// Use -matchstring=<name> to produce a function returning the same result as
//
//	regexp.MustCompile(expr).MatchString(s)
//
// except the Match machine interprets 's' as ASCII.
//
// # The MatchUtf8 machine
//
// Use -matchutf8=<name> to produce a function returning the same result as
//
//	regexp.MustCompile(expr).Match(s)
//
// The machine interprets 's' as UTF-8 encoded text.
//
// # The MatchStringUtf8 machine
//
// Use -matchstringutf8=<name> to produce a function returning the same result as
//
//	regexp.MustCompile(expr).MatchString(s)
//
// The machine interprets 's' as UTF-8 encoded text.
package main // impot "modernc.org/rec"

import (
	"fmt"
	"os"
	"strings"

	"modernc.org/rec/lib"
)

func abort(rc int, msg string, args ...interface{}) {
	fmt.Fprintln(os.Stderr, "FAIL: "+strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	rc, err := rec.Main(os.Args[1:], os.Stdout, os.Stderr)
	switch {
	case rc != 0 && err == nil:
		abort(rc, "")
	case rc != 0:
		abort(rc, "%v", err)
	}
}
