# rec

Command rec compiles a subset of Go regexps to Go code.

Documentation: [godoc.org/modernc.org/rec](http://godoc.org/modernc.org/rec)
