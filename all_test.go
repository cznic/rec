// Copyright 2023 The Rec Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main // modernc.org/rec

import (
	"flag"
	"fmt"
	goscanner "go/scanner"
	"go/token"
	"io/fs"
	"math"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"testing"

	"github.com/dustin/go-humanize"
	mc "modernc.org/regexp"
)

var (
	oRE = flag.String("re", "", "")
	re  *regexp.Regexp
)

func TestMain(m *testing.M) {
	flag.Parse()
	if *oRE != "" {
		re = regexp.MustCompile(*oRE)
	}
	rc := m.Run()
	os.Exit(rc)
}

// origin returns caller's short position, skipping skip frames.
func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
		if strings.HasPrefix(fns, "func") {
			num := true
			for _, c := range fns[len("func"):] {
				if c < '0' || c > '9' {
					num = false
					break
				}
			}
			if num {
				return origin(skip + 2)
			}
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

// todo prints and returns caller's position and an optional message tagged with TODO. Output goes to stderr.
//
//lint:ignore U1000 whatever
func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	s = fmt.Sprintf("%s\n\tTODO %s", origin(2), s)
	// fmt.Fprintf(os.Stderr, "%s\n", s)
	// os.Stdout.Sync()
	return s
}

// trc prints and returns caller's position and an optional message tagged with TRC. Output goes to stderr.
//
//lint:ignore U1000 whatever
func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	s = fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", s)
	os.Stderr.Sync()
	return s
}

//lint:ignore U1000 whatever
func stack() []byte { return debug.Stack() }

//lint:ignore U1000 whatever
func line() int {
	_, _, line, _ := runtime.Caller(1)
	return line
}

var testMatchData = []byte(`Jan 18 06:41:30 corecompute sshd[42327]: Failed keyboard-interactive/pam for root from 112.100.68.182 port 48803 ssh2`)

var testMatchsink int

func TestMatch(t *testing.T) {
	t.Run("match", func(t *testing.T) { runTestMatch(t) })
	t.Run("match2", func(t *testing.T) { runTestMatch2(t) })
	t.Run("match3", func(t *testing.T) { runTestMatch3(t) })
	t.Run("match4", func(t *testing.T) { runTestMatch4(t) })
}

func runTestMatch(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatch2(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch2(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatch3(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch3(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatch4(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch4(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatalf("#%d: got %v, exp %v, data %q", i, g, e, d)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func TestMatchString(t *testing.T) {
	t.Run("match", func(t *testing.T) { runTestMatchString(t) })
	t.Run("match2", func(t *testing.T) { runTestMatchString2(t) })
	t.Run("match3", func(t *testing.T) { runTestMatchString3(t) })
	t.Run("match4", func(t *testing.T) { runTestMatchString4(t) })
}

func runTestMatchString(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatchString(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchString2(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatchString2(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchString3(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatchString3(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchString4(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatchString4(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func TestMatchUtf8(t *testing.T) {
	t.Run("match", func(t *testing.T) { runTestMatchUtf8(t) })
	t.Run("match2", func(t *testing.T) { runTestMatchUtf82(t) })
	t.Run("match3", func(t *testing.T) { runTestMatchUtf83(t) })
	t.Run("match4", func(t *testing.T) { runTestMatchUtf84(t) })
}

func runTestMatchUtf8(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatchUtf8(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchUtf82(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch2Utf8(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchUtf83(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch3Utf8(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchUtf84(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	for i := 0; i < max; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}
		g, e := testMatch4Utf8(d), re.Match(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatalf("#%d: got %v, exp %v, data %q", i, g, e, d)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func TestMatchStringUtf8(t *testing.T) {
	t.Run("match", func(t *testing.T) { runTestMatchStringUtf8(t) })
	t.Run("match2", func(t *testing.T) { runTestMatchStringUtf82(t) })
	t.Run("match3", func(t *testing.T) { runTestMatchStringUtf83(t) })
	t.Run("match4", func(t *testing.T) { runTestMatchStringUtf84(t) })
}

func runTestMatchStringUtf8(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatchStringUtf8(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchStringUtf82(t *testing.T) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatch2StringUtf8(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchStringUtf83(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatch3StringUtf8(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func runTestMatchStringUtf84(t *testing.T) {
	re := regexp.MustCompile(`\Asshd\[\d{5}\]:\s*Failed\z`)
	var hits, hits2 int
	const max = 1e6
	s := string(testMatchData)
	for i := 0; i < max; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}
		g, e := testMatch4StringUtf8(d), re.MatchString(d)
		if g {
			hits++
		}
		if e {
			hits2++
		}
		if g != e {
			t.Fatal(i, g, e)
		}
	}
	if g, e := hits, hits2; g != e {
		t.Fatal(g, e)
	}
}

func TestLex(t *testing.T) {
	t.Run("lex", func(t *testing.T) { runTestLex(t, func(s string) (int, int) { return testLex([]byte(s)) }) })
	t.Run("lexstring", func(t *testing.T) { runTestLex(t, func(s string) (int, int) { return testLexString(s) }) })
	t.Run("lexutf8", func(t *testing.T) { runTestLex(t, func(s string) (int, int) { return testLexUtf8([]byte(s)) }) })
	t.Run("lexstringutf8", func(t *testing.T) { runTestLex(t, func(s string) (int, int) { return testLexStringUtf8(s) }) })
}

func runTestLex(t *testing.T, f func(s string) (int, int)) {
	for i, v := range []struct {
		src  string
		n, m int
	}{
		{"", -1, 0},
		{"?", -1, 0},
		{"a", 0, 1},
		{"0", 1, 1},
		{"ab", 0, 2},
		{"01", 1, 2},
		{"a?", 0, 1},
		{"0?", 1, 1},
	} {
		n, m := f(v.src)
		if n != v.n || m != v.m {
			t.Errorf("%3d %q: got %d %d, exp %d %d\n", i, v.src, n, m, v.n, v.m)
			continue
		}

		t.Logf("%3d %q: %d %d\n", i, v.src, n, m)
	}
}

func BenchmarkMatch(b *testing.B) {
	b.Run("stdlib", func(b *testing.B) { benchmarkMatchStdlib(b) })
	b.Run("modernc", func(b *testing.B) { benchmarkMatchModernc(b) })
	b.Run("rec", func(b *testing.B) { benchmarkMatchRec(b) })
	b.Run("rec/string", func(b *testing.B) { benchmarkMatchStringRec(b) })
	b.Run("rec/utf8", func(b *testing.B) { benchmarkMatchRecUtf8(b) })
	b.Run("rec/stringutf8", func(b *testing.B) { benchmarkMatchStringUtf8Rec(b) })
}

func benchmarkMatchStdlib(b *testing.B) {
	re := regexp.MustCompile(`sshd\[\d{5}\]:\s*Failed`)
	var hits int
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}

		if re.Match(d) {
			hits++
		}
	}
	testMatchsink += hits
}

func benchmarkMatchModernc(b *testing.B) {
	re := mc.MustCompile(`sshd\[\d{5}\]:\s*Failed`)
	var hits int
	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}

		if re.Match(d) {
			hits++
		}
	}
	testMatchsink += hits
}

func benchmarkMatchRec(b *testing.B) {
	var hits int
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}

		if testMatch(d) {
			hits++
		}
	}
	testMatchsink += hits
}

func benchmarkMatchStringRec(b *testing.B) {
	var hits int
	s := string(testMatchData)
	b.ResetTimer()
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}

		if testMatchString(d) {
			hits++
		}
	}
	testMatchsink += hits
}

func benchmarkMatchRecUtf8(b *testing.B) {
	var hits int
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		var d []byte
		if i >= len(testMatchData) {
			d = testMatchData[i%len(testMatchData):]
		} else {
			d = testMatchData[:i]
		}

		if testMatchUtf8(d) {
			hits++
		}
	}
	testMatchsink += hits
}

func benchmarkMatchStringUtf8Rec(b *testing.B) {
	var hits int
	s := string(testMatchData)
	b.ResetTimer()
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		var d string
		if i >= len(testMatchData) {
			d = s[i%len(testMatchData):]
		} else {
			d = s[:i]
		}

		if testMatchStringUtf8(d) {
			hits++
		}
	}
	testMatchsink += hits
}

func h(v interface{}) string {
	switch x := v.(type) {
	case int:
		return humanize.Comma(int64(x))
	case int32:
		return humanize.Comma(int64(x))
	case int64:
		return humanize.Comma(x)
	case uint32:
		return humanize.Comma(int64(x))
	case uint64:
		if x <= math.MaxInt64 {
			return humanize.Comma(int64(x))
		}
	}
	return fmt.Sprint(v)
}

var (
	keywords = map[string]token.Token{
		"break":       token.BREAK,
		"case":        token.CASE,
		"chan":        token.CHAN,
		"const":       token.CONST,
		"continue":    token.CONTINUE,
		"default":     token.DEFAULT,
		"defer":       token.DEFER,
		"else":        token.ELSE,
		"fallthrough": token.FALLTHROUGH,
		"for":         token.FOR,
		"func":        token.FUNC,
		"go":          token.GO,
		"goto":        token.GOTO,
		"if":          token.IF,
		"import":      token.IMPORT,
		"interface":   token.INTERFACE,
		"map":         token.MAP,
		"package":     token.PACKAGE,
		"range":       token.RANGE,
		"return":      token.RETURN,
		"select":      token.SELECT,
		"struct":      token.STRUCT,
		"switch":      token.SWITCH,
		"type":        token.TYPE,
		"var":         token.VAR,
	}
	xlat = [...]token.Token{
		token.EOF,

		token.CHAR,
		token.COMMENT,
		token.INT,
		token.FLOAT,
		token.IDENT,
		token.IMAG,
		token.STRING,

		token.ADD, token.ADD_ASSIGN, token.INC,
		token.AND, token.LAND, token.AND_ASSIGN, token.AND_NOT, token.AND_NOT_ASSIGN,
		token.ASSIGN, token.EQL,
		token.DEFINE, token.COLON,
		token.GTR, token.GEQ, token.SHR, token.SHR_ASSIGN,
		token.LBRACE, token.RBRACE,
		token.LBRACK, token.RBRACK,
		token.LPAREN, token.RPAREN,
		token.LSS, token.LEQ, token.ARROW, token.SHL, token.SHL_ASSIGN,
		token.MUL, token.MUL_ASSIGN,
		token.NOT, token.NEQ,
		token.OR, token.LOR, token.OR_ASSIGN,
		token.PERIOD, token.ELLIPSIS,
		token.QUO, token.QUO_ASSIGN,
		token.REM, token.REM_ASSIGN,
		token.SUB, token.SUB_ASSIGN, token.DEC,
		token.XOR, token.XOR_ASSIGN,

		token.COMMA,
		token.SEMICOLON,
		token.TILDE,
	}
)

func TestGoScanner(t *testing.T) {
	var files, nbytes, tokens int
	if err := filepath.Walk(runtime.GOROOT(), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() || !strings.HasSuffix(path, ".go") {
			return nil
		}

		if re != nil && !re.MatchString(path) {
			return nil
		}

		b, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		nbytes += len(b)
		var gs goscanner.Scanner
		fs := token.NewFileSet()
		gs.Init(fs.AddFile(path, -1, len(b)), b, nil, 0)
		srcPos := 0
		files++
	out:
		for {
			goPos0, goTok, goLit := gs.Scan()
			if goTok == token.ILLEGAL {
				break
			}

			goPos := fs.Position(token.Pos(goPos0))
			switch goTok {
			case token.SEMICOLON:
				if goLit == "\n" {
					continue
				}
			case token.CHAR:
				if _, _, _, err = strconv.UnquoteChar(goLit, '\''); err != nil {
					break out
				}
			case token.STRING:
				if _, err = strconv.Unquote(goLit); err != nil {
					break out
				}
			}
		more:
			pos := fs.Position(token.Pos(srcPos + 1))
			id, length := scanner(b[srcPos:])
			lit := string(b[srcPos : srcPos+length])
			tok := token.ILLEGAL
			if id >= 0 && id < len(xlat) {
				tok = xlat[id]
			}

			switch tok {
			case token.COMMENT:
				srcPos += length
				goto more
			case token.IDENT:
				if k, ok := keywords[lit]; ok {
					tok = k
				}
			case token.CHAR:
				// go/scanner treats invalid ''' token differently
				if lit == "'''" {
					lit = "''"
					length--
				}
			}

			tokens++
			if g, e := tok, goTok; g != e {
				t.Fatalf("%d: %s(%s): %v %q, got id %v, length %v, tok %v, %q", files, pos, goPos, goTok, goLit, id, length, tok, lit)
			}

			if g, e := lit, goLit; e != "" && g != e {
				t.Fatalf("%d: %s(%s): %v %q, got id %v, length %v, tok %v, %q", files, pos, goPos, goTok, goLit, id, length, tok, lit)
			}

			if goTok == token.EOF {
				break
			}

			srcPos += length
		}

		return nil
	}); err != nil {
		t.Fatal(err)
	}
	t.Logf("files %s, bytes %s, tokens %s", h(files), h(nbytes), h(tokens))
}

type input struct {
	path string
	b    []byte
}

func BenchmarkScanner(b *testing.B) {
	var inputs []input
	if err := filepath.Walk(runtime.GOROOT(), func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() || !strings.HasSuffix(path, ".go") {
			return nil
		}

		b, err := os.ReadFile(path)
		if err != nil {
			return err
		}

		inputs = append(inputs, input{path, b})
		return nil
	}); err != nil {
		b.Fatal(err)
	}

	b.Run("stdlib", func(b *testing.B) { benchmarkStdlibScanner(b, inputs) })
	b.Run("rec", func(b *testing.B) { benchmarkRecScanner(b, inputs) })
}

func benchmarkStdlibScanner(b *testing.B, inputs []input) {
	var nbytes int
	var s goscanner.Scanner
	fs := token.NewFileSet()
	debug.FreeOSMemory()
	b.ReportAllocs()
	b.ResetTimer()
	var tok token.Token
	for i := 0; i < b.N; i++ {
		nbytes = 0
		for _, input := range inputs {
			nbytes += len(input.b)
			s.Init(fs.AddFile(input.path, -1, len(input.b)), input.b, nil, goscanner.ScanComments)
			for {
				if _, tok, stringSink = s.Scan(); tok == token.EOF {
					break
				}
			}
		}
	}
	b.SetBytes(int64(nbytes))
}

var stringSink string

func benchmarkRecScanner(b *testing.B, inputs []input) {
	var nbytes int
	fs := token.NewFileSet()
	debug.FreeOSMemory()
	b.ReportAllocs()
	b.ResetTimer()
	var tok, length int
	for i := 0; i < b.N; i++ {
		nbytes = 0
		for _, input := range inputs {
			b := input.b
			nbytes += len(b)
			file := fs.AddFile(input.path, -1, len(b))
			pos := 0
		loop:
			for {
				switch tok, length = scanner(b[pos:]); tok {
				case -1: // token.ILLEGAL
					pos++
					continue
				case 0: // token.EOF
					break loop
				case 2: // token.COMMENT
					if length == 1 && b[pos] == '\n' {
						file.AddLine(pos)
					}
				}

				stringSink = string(b[pos : pos+length])
				pos += length
			}
		}
	}
	b.SetBytes(int64(nbytes))
}
