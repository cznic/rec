module modernc.org/rec

go 1.23

require (
	github.com/dustin/go-humanize v1.0.1
	modernc.org/gc/v3 v3.0.0-20250225134559-fd9931328834
	modernc.org/regexp v1.7.9
)

require (
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	modernc.org/fsm v1.3.2 // indirect
	modernc.org/mathutil v1.7.1 // indirect
	modernc.org/sortutil v1.2.1 // indirect
	modernc.org/strutil v1.2.1 // indirect
	modernc.org/token v1.1.0 // indirect
)
